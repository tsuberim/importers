export interface Status {
  isOpen: boolean
}

export interface Command {
  type: string
}
export interface CloseCommand {
  type: 'close'
}
export interface OpenCommand {
  type: 'open'
}

export const importerURLs = []

export async function getImporterStatus(url: string, command?: Command): Promise<Status> {
  return { isOpen: true }
}

export interface IFC {
  open: boolean;
  status: string,
  lastActiveTime: number;
  lastCreationTime: number;
  levConnection: string;
  sensorConnection: string;
  missionId: number,

  components: Component[]
}

export interface Component {
  status: string,
  id: string;
  firstActiveTime: number;
  lastActiveTime: number;
  creationTime: number;
  family: string;
  frequency: number,
  pri: number;
}

const ifcs: IFC[] = [
  {
    open: false,
    status: 'פתוח',
    lastActiveTime: new Date().getTime(),
    lastCreationTime: new Date().getTime(),
    levConnection: 'תקין',
    sensorConnection: 'תקין',
    missionId: 123,
    components: []
  },
  {
    open: false,
    status: 'פתוח',
    lastActiveTime: new Date().getTime(),
    lastCreationTime: new Date().getTime(),
    levConnection: 'תקין',
    sensorConnection: 'תקין',
    missionId: 123,
    components: [{
      status: 'פעיל',
      id: '394562843756',
      firstActiveTime: new Date().getTime(),
      lastActiveTime: new Date().getTime(),
      creationTime: new Date().getTime(),
      family: 'ASDFA_AS',
      frequency: 123.213,
      pri: 12.1248,
    },
    {
      status: 'פעיל',
      id: '394562843756',
      firstActiveTime: new Date().getTime(),
      lastActiveTime: new Date().getTime(),
      creationTime: new Date().getTime(),
      family: 'ASDFA_AS',
      frequency: 123.213,
      pri: 12.1248,
    }]
  },
]

// setInterval(() => {
//   for (const ifc of ifcs) {
//     ifc.open = Math.random() <= 0.7
//     ifc.lastActiveTime = new Date().getTime()
//     ifc.missionId++;

//     for (const comp of ifc.components) {
//       comp.status = Math.random() <= 0.7 ? 'פעיל' : 'תקול'
//       comp.lastActiveTime = new Date().getTime()
//       comp.frequency = Math.random() * 100
//       comp.pri = Math.random() * 10
//     }
//   }
// }, 5000)

export async function getIfcs(): Promise<IFC[]> {
  console.log('getIfcs')
  await new Promise(res => setTimeout(res, 1000 * Math.random()))
  console.log(ifcs)
  return ifcs
}

export async function setIfcOpenStatus(idx: number, open: boolean) {
  ifcs[idx].open = open;
  return ifcs[idx]
}