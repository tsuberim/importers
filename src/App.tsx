import React, { useState } from "react";
import { Header, Container, Checkbox, Segment, Menu, Table, Accordion } from "semantic-ui-react";
import { AsyncAction } from "./utils";
import { stat } from "fs";
import { getIfcs, setIfcOpenStatus } from "./defs";
import moment from 'moment';

function App() {

  const [prod, setProd] = useState(false);

  return (<>
    <Segment vertical>
      <Container>
        <Menu secondary>
          <Menu.Item as='a' active={prod} color='red' onClick={() => setProd(true)}>
            סביבה מבצעית
          </Menu.Item>
          <Menu.Item as='a' active={!prod} color='orange' onClick={() => setProd(false)}>
            סביבה תרגילית
          </Menu.Item>
        </Menu>
      </Container>
    </Segment>
    <Segment vertical>
      <Container>
        <Header>גישורים</Header>
        <AsyncAction immidiate period={500} fetch={getIfcs} render={(status, result, getIfcs) =>
          <Table textAlign='right'>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>
                  פתוח
                </Table.HeaderCell>
                <Table.HeaderCell>
                  סטטוס פעילות
              </Table.HeaderCell>
                <Table.HeaderCell>
                  זמן פעילות אחרון
              </Table.HeaderCell>
                <Table.HeaderCell>
                  זמן יצירה
              </Table.HeaderCell>
                <Table.HeaderCell>
                  קשר מול לב
              </Table.HeaderCell>
                <Table.HeaderCell>
                  קשר מול סנסור
              </Table.HeaderCell>
                <Table.HeaderCell>
                  מספר משימה
              </Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {result && result.length == 0 ?
                <Table.Row textAlign='center'>
                  <Table.Cell as='td' colspan={7}>
                    אין מידע
                    </Table.Cell>
                </Table.Row>
                : <></>}
              {result && result.map(({ open, status, lastActiveTime, lastCreationTime, levConnection, missionId, sensorConnection, components }, idx) => <>
                <Table.Row>
                  <Table.Cell>
                    <AsyncAction resetTimeoutOnFailure={1} resetTimeoutOnSuccess={1} fetch={async (open) => {
                      const result = await setIfcOpenStatus(idx, !open);
                      return result
                    }} render={(status, result, fetch) =>
                      <Checkbox toggle checked={open} onChange={(e, { toggle }) => fetch(toggle)} />
                    }
                    />
                  </Table.Cell>
                  <Table.Cell>
                    {open ? 'פתוח' : 'סגור'}
                  </Table.Cell>
                  <Table.Cell>
                    {moment(lastActiveTime).format('YYYY-MM-DD HH:mm:ss')}
                  </Table.Cell>
                  <Table.Cell>
                    {moment(lastCreationTime).format('YYYY-MM-DD HH:mm:ss')}
                  </Table.Cell>
                  <Table.Cell>
                    {levConnection}
                  </Table.Cell>
                  <Table.Cell>
                    {sensorConnection}
                  </Table.Cell>
                  <Table.Cell>
                    {missionId}
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell as='td' colspan={7}>
                    <Table textAlign='right'>
                      <Table.Row>
                        <Table.HeaderCell>
                          סטטוס פעילות
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          מזהה קליטה
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          זמן פעילות ראשון
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          זמן פעילות אחרון
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          זמן יצירה
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          משפחה
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          תדר
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          קצב
                          </Table.HeaderCell>
                      </Table.Row>
                      {components.length == 0 ?
                        <Table.Row textAlign='center'>
                          <Table.Cell as='td' colspan={8}>
                            אין אמצעי איסוף
                          </Table.Cell>
                        </Table.Row>
                        : <></>}
                      {components.map(({ status, id, firstActiveTime, lastActiveTime, creationTime, family, frequency, pri }) =>
                        <Table.Row>
                          <Table.Cell>
                            {status}
                          </Table.Cell>
                          <Table.Cell>
                            {id}
                          </Table.Cell>
                          <Table.Cell>
                            {moment(firstActiveTime).format('YYYY-MM-DD HH:mm:ss')}
                          </Table.Cell>
                          <Table.Cell>
                            {moment(lastActiveTime).format('YYYY-MM-DD HH:mm:ss')}
                          </Table.Cell>
                          <Table.Cell>
                            {moment(creationTime).format('YYYY-MM-DD HH:mm:ss')}
                          </Table.Cell>
                          <Table.Cell>
                            {family}
                          </Table.Cell>
                          <Table.Cell>
                            {frequency.toFixed(3)}
                          </Table.Cell>
                          <Table.Cell>
                            {pri.toFixed(3)}
                          </Table.Cell>
                        </Table.Row>
                      )}
                    </Table>
                  </Table.Cell>
                </Table.Row>
              </>)}
            </Table.Body>
          </Table>
        } />
        <Header>יבואנים</Header>
        <AsyncAction immidiate period={500} fetch={getIfcs} render={(status, result, getIfcs) =>
          <Table textAlign='right'>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>
                  פתוח
                </Table.HeaderCell>
                <Table.HeaderCell>
                  סטטוס פעילות
              </Table.HeaderCell>
                <Table.HeaderCell>
                  זמן פעילות אחרון
              </Table.HeaderCell>
                <Table.HeaderCell>
                  זמן יצירה
              </Table.HeaderCell>
                <Table.HeaderCell>
                  קשר מול לב
              </Table.HeaderCell>
                <Table.HeaderCell>
                  קשר מול סנסור
              </Table.HeaderCell>
                <Table.HeaderCell>
                  מספר משימה
              </Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {result && result.length == 0 ?
                <Table.Row textAlign='center'>
                  <Table.Cell as='td' colspan={7}>
                    אין מידע
                    </Table.Cell>
                </Table.Row>
                : <></>}
              {result && result.map(({ open, status, lastActiveTime, lastCreationTime, levConnection, missionId, sensorConnection, components }, idx) => <>
                <Table.Row>
                  <Table.Cell>
                    <AsyncAction resetTimeoutOnFailure={1} resetTimeoutOnSuccess={1} fetch={async (open) => {
                      const result = await setIfcOpenStatus(idx, !open);
                      return result
                    }} render={(status, result, fetch) =>
                      <Checkbox toggle checked={open} onChange={(e, { toggle }) => fetch(toggle)} />
                    }
                    />
                  </Table.Cell>
                  <Table.Cell>
                    {status}
                  </Table.Cell>
                  <Table.Cell>
                    {moment(lastActiveTime).format('YYYY-MM-DD HH:mm:ss')}
                  </Table.Cell>
                  <Table.Cell>
                    {moment(lastCreationTime).format('YYYY-MM-DD HH:mm:ss')}
                  </Table.Cell>
                  <Table.Cell>
                    {levConnection}
                  </Table.Cell>
                  <Table.Cell>
                    {sensorConnection}
                  </Table.Cell>
                  <Table.Cell>
                    {missionId}
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell as='td' colspan={7}>
                    <Table textAlign='right'>
                      <Table.Row>
                        <Table.HeaderCell>
                          סטטוס פעילות
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          מזהה קליטה
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          זמן פעילות ראשון
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          זמן פעילות אחרון
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          זמן יצירה
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          משפחה
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          תדר
                          </Table.HeaderCell>
                        <Table.HeaderCell>
                          קצב
                          </Table.HeaderCell>
                      </Table.Row>
                      {components.length == 0 ?
                        <Table.Row textAlign='center'>
                          <Table.Cell as='td' colspan={7}>
                            אין אמצעי איסוף
                          </Table.Cell>
                        </Table.Row>
                        : <></>}
                      {components.map(({ status, id, firstActiveTime, lastActiveTime, creationTime, family, frequency, pri }) =>
                        <Table.Row>
                          <Table.Cell>
                            {status}
                          </Table.Cell>
                          <Table.Cell>
                            {id}
                          </Table.Cell>
                          <Table.Cell>
                            {moment(firstActiveTime).format('YYYY-MM-DD HH:mm:ss')}
                          </Table.Cell>
                          <Table.Cell>
                            {moment(lastActiveTime).format('YYYY-MM-DD HH:mm:ss')}
                          </Table.Cell>
                          <Table.Cell>
                            {moment(creationTime).format('YYYY-MM-DD HH:mm:ss')}
                          </Table.Cell>
                          <Table.Cell>
                            {family}
                          </Table.Cell>
                          <Table.Cell>
                            {frequency}
                          </Table.Cell>
                          <Table.Cell>
                            {pri}
                          </Table.Cell>
                        </Table.Row>
                      )}
                    </Table>
                  </Table.Cell>
                </Table.Row>
              </>)}
            </Table.Body>
          </Table>
        } />
      </Container>
    </Segment>
  </>
  );
}

export default App;
