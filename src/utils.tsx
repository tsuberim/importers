import { useState, useEffect } from "react"

export interface Idle {
  type: 'idle',
}
export interface Pending {
  type: 'pending',
}
export interface Failure {
  type: 'failure',
  error: Error
}
export interface Success {
  type: 'success',
}

export type AsyncStatus = Idle | Pending | Failure | Success

export interface AsyncProps<T> {
  fetch: (...args: any[]) => Promise<T>,
  immidiate?: boolean,
  period?: number,
  resetTimeoutOnSuccess?: number,
  resetTimeoutOnFailure?: number,
  render: (status: AsyncStatus, result: T | null, fetch: (...args: any[]) => Promise<T | null>) => JSX.Element;
}

export function AsyncAction<T>({ render, immidiate, period, resetTimeoutOnFailure, resetTimeoutOnSuccess, fetch: action }: AsyncProps<T>) {
  const [status, setStatus] = useState<AsyncStatus>({ type: 'idle' });
  const [result, setResult] = useState<T | null>(null);

  async function act(args: any[], force = false): Promise<T | null> {
    if (status.type === 'idle' || force) {
      try {
        setStatus({ type: 'pending' })
        const result = await action(...args)
        setStatus({ type: 'success' })
        setResult(result)
        if (resetTimeoutOnSuccess) {
          setTimeout(() => setStatus({ type: 'idle' }), resetTimeoutOnSuccess)
        }
        return result;
      } catch (error) {
        setStatus({ type: 'failure', error })
        if (resetTimeoutOnFailure) {
          setTimeout(() => setStatus({ type: 'idle' }), resetTimeoutOnFailure)
        }
      }
    }
    return null;
  }

  useEffect(() => {
    if (immidiate) {
      act([])
    }

    if (period) {
      const handle = setInterval(() => act([], true), period);
      return () => clearInterval(handle);
    }
  })

  return render(status, result, (...args) => act(args))
}
